<figure class="image-right"> <img width="50%" src="images/Projects/calindori.png" alt="Calindori" /><figcaption>Calindori.</figcaption></figure>

[Calindori 1.2](https://apps.kde.org/calindori/) is the first stable release of Calindori as a KDE application. Calindori is a touch friendly calendar application. It has been designed for mobile devices but it can also run on desktop environments. Users of Calindori can check previous and future dates and manage tasks and events.

When executing the application for the first time, a new calendar file is created that follows the ical standard. Alternatively, users may create additional calendars or import existing ones.

Calindori is available on [KDE Neon](https://neon.kde.org/) for [Plasma Mobile](https://www.plasma-mobile.org/) as well as on [postmartketOS](<https://postmarketos.org/>) and it also works on Linux mobile, desktop and even Android. It’s also available in the flaptpak nightlies kdeapps repository for ARM and x84_64. Finally, you can also build it from source on your Linux desktop workstation.

### Features

* Agenda
* Events
* Todos
* Multiple calendars
* Import calendar
