<div class="text-center">
    <figure class="m-3 p-3"><img src="./images/Onboarding/GroupPhoto1.jpg" width="100%" /><br /></figure>
</div>

Between the 19th and 23rd of July 2019, around 20 KDE contributors met in Nuremberg, Germany, to work on 3 different projects, in what turned out to be a KDE Megasprint hosted by SUSE. KDE's Onboarding Sprint was one of the projects. The goal of the sprint was to answer the question "how do we make it easier for people to start contributing?"

### The Streamlined Onboarding Goal Sprint

This sprint took place as part of KDE's <a href="https://community.kde.org/Goals/Streamlined_onboarding_of_new_contributors">Streamlined Onboarding</a> goal, a goal that had been running for a year and a half at that point. Its focus was on making it easier for new contributors to set up a development environment and get started with developing KDE software.

### Identify use cases

The next step was to list, compare and contrast all the ways that were available to developers at the time to get started with coding KDE software.

We considered implementations involving distribution packages, cmake external projects, docker built with dependencies from the binary factory, docker with kdesrc-build, kdesrc-build, craft and flatpaks.

It quickly became evident that finding a solution that would fit everyone's needs would be very hard, if not impossible. To be able to proceed, we had to narrow it down to five major use cases:


+ Applications (including Frameworks)
+ Self-contained (e.g. Krita)
+ Using multiple repositories (e.g. PIM)
+ Plasma-Desktop
+ Plasma-Mobile

      
The prevailing impression was that each use case would require a different approach for setting up a development environment. However, as the goal of the sprint was to enable new contributors and in particular those that would not have much experience with KDE's development process and tools, we decided that we would focus on use case #1.

KDE's Applications, and self-contained ones, in particular, are a great entry point for new people to get involved, and that is where we should be pointing newcomers that are just getting started. Hence, we decided that would be the case we would be tackling first.

<div class="text-center">
    <figure class="m-3 p-3"><img src="./images/Onboarding/Davids.jpg" width="800" /><br /><figcaption>How many Davids does it take to fix a bug?</figcaption></figure>
</div>

### The requirements

The discussion that followed revolved around the design and the challenges that a good solution would solve, as it should be able to:

+ resolve the dependencies, by providing a set of pre-built binary dependencies that is as rich as possible.
+ configure the tools developers need to pre-install before they can get started (e.g. a compiler, git, cmake).
+ avoid using a command-line interface and offer IDE integration.
+ download the sources, and then build and run the relevant application.


The discussion came down to the story that we wished to tell developers that were getting started. What would the path be we wanted them to walk in order to start contributing code? Did we want Linux-based operating systems to be our go-to OS for development? Would we ask newcomers to use KDevelop as the go-to IDE? How did developers download a project and set up environment variables?

Several of those questions were not given a clear answer. To be able to have a developer story, we needed to offer a solid starting experience and make some choices that, even though they might have limitations, they would best serve the purpose.

### Work on prototypes for possible solutions

The result of the discussions that took place on the first day was that the two most feasible solutions would involve the usage of either Conan or Flatpak, as they seemed to give answers to the emerging question of automating the resolving of dependencies, in addition to other features that each offer.

On the second day, the participating developers formed smaller groups to test the adoption of each of these solutions to our needs. Most of the work went into using each tool to build a working prototype that would help with getting started with coding a KDE application on it. A small and simple KDE application (Kruler) with not many dependencies was chosen for this purpose.

Conan is an open-source C/C++ package manager for developers. Ovidiu-Florin Bogdan has significant experience with it, so he was able to explain the available features, respond to questions and share his insight on the possible implementation. Together with Dmitry Kazakov from the Krita team, they worked on preparing a Conan solution and documenting the first steps and challenges that this involved.

Flatpak is a utility for building and deploying software and managing packages on Linux-based operating systems. Aleix Pol had put the time towards supporting this technology in KDevelop, but there was still work left to be done. Together with Albert Astals Cid and David Redondo, they worked on testing the status of this implementation and recorded the pending challenges which we had to address if we wanted to bring this solution to a state that would serve our needs.

<div class="text-center">
    <figure class="m-3 p-3"><img src="./images/Onboarding/KolourPaintVR.jpg" width="800" /><br /><figcaption>Painting with KolourPaint on a VR canvas.</figcaption></figure>
</div>

### Moving forward

The sprint helped kick-start the discussion about making it easy for newcomers to set up a development environment. The majority of KDE developers that attended the sprint seemed to understand the importance of achieving this objective, but at the same time were concerned about the difficulties that such a task involves.

The major challenge moving forward will be to support the developers to put further work into the prototypes so we can have a fully working solution to test and put to use to our advantage.
