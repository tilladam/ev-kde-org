[Angelfish](https://apps.kde.org/en-gb/angelfish/) is a modern lightweight web browser that will work on your desktop and your Plasma Mobile device.

It supports typical browser features, such as

* bookmarks
* history
* tabs
