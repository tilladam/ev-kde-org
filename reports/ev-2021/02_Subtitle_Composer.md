We love making videos, and, to make those videos accessible, it's great to have subtitles in them. [Subtitle Composer](https://subtitlecomposer.kde.org/) is a new app from KDE that helps you add texts to your videos. You can set the timing, font, size and add multiple languages.

For translations and transcriptions, you can work with two subtitles side-by-side. Subtitle Composer lets you adapt the waveform to your liking, fit your subtitles to your needs and change the interface itself! The user interface is adaptable, so you can use whatever colours work best with the video and you can move panels to find a workflow that is the most comfortable for you.

It works with all the subtitle file formats, including text and graphical subtitle formats, formats supported by ffmpeg, demux formats, MicroDVD, and graphical formats supported by ffmpeg.

It does speech recognition too, so it can even add the subtitles for you. And you can quickly and easily sync subtitles by dragging several anchors/graftpoints and stretching the timeline, doing time shifting and scaling, lines duration re-calculation, framerate conversion then joining and splitting the subtitle files. Spell checking is included, of course, and it can even be scripted in multiple languages.
