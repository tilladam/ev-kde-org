Powered by a modified Rockchip RK3399 hexa-core SoC operating at 1.5GHz, the [PinePhone Pro](https://www.pine64.org/pinephonepro/) was ready for ordering in November. The PinePhone Pro ships with 4GB of dual-channel LPDDR4 RAM as well as 128GB of internal eMMC flash storage. It features a high-fidelity 13MP main camera sensor and a 5MP front-facing camera.

The PinePhone Pro is the device of choice to test run KDE's [Plasma Mobile](https://plasma-mobile.org/), an entirely free and open sourced environment for mobile devices.
