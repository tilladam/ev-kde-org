[Koko](https://apps.kde.org/en-gb/koko/) is an image gallery application designed for Plasma Mobile that can be used to view, edit and share images.

With Koko, users can:

* browse the images of the standard Pictures directory. If the user chooses a collection then a new column is pushed into the row and if he/she chooses an image then the chosen image is shown in high resolution.
* select the images to browse using various filters. The user can select the filters from the left sidebar.
* select multiple images at a time and delete or share them (just the images and not the collections or the folders).
* view images of a folder in a higher resolution. Images can be viewed in windowed mode as well as full screen mode which can be toggled by pressing <kbr>F</kbr>.
