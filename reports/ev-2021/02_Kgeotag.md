Another new app released in 2021 was [KGeoTag](https://www.kphotoalbum.org/2021/02/27/0103/). You can use KGeoTag to assign image files to GPS locations on a map. This can help you remember the exact location of where a photo was taken, or discover images that were taken at the same place. Of course, this is most useful when used together with another program such as KPhotoAlbum, that can also display this information and lets you search by GPS coordinates.

With KGeoTag you can

* match image coordinates to GPX track files based on time information
* drag and drop images to points on the map
* store bookmarks for frequently used places
* set the elevation (manually or by looking it up online).
