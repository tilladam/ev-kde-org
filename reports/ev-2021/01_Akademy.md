[Akademy](https://akademy.kde.org/2021) 2021 was hosted online from the 18th to the 25th of June, 2021.

The annual conference of the KDE Community this year featured training sessions, presentations on the latest KDE developments, talks, workshops, Birds of a Feather (BoF) sessions, hacking and coding sessions. Akademy this year welcomed more than 86 speakers, attracted 476 attendees on BigBlueButton, 1248 viewers on YouTube and reached the global KDE Community and beyond.

### Day 2

#### Talks, Lightning Talks & Panels

First thing in the morning, Aleix Pol, president of KDE, introduced the event and explained how Akademy 2021 was still "special" because we could still not meet in person.

Aleix then introduced the first keynote speaker of the event: Patricia Aas, co-founder of TurtleSec and a C++ developer and trainer. In her talk, Patricia explored bugs, types of bugs, how to deal with the secondary problems bugs create.

Then Aleix was back with his own talk, in which he told us about KDE's ["All about the apps" goal](https://community.kde.org/Goals/All_about_the_Apps). The goal encourages community members to develop, improve and help distribute KDE apps. This it turns out, is especially important for new KDE environments, such as Plasma Mobile! He explained how the goal is going and encouraged people to join the effort.

Niccolò Venerandi took over and talked about [the "Consistency" goal](https://community.kde.org/Goals/Consistency). The Consistency goal seeks to make sure all KDE elements integrate well with each other, and makes sure the look and feel of apps and environments are similar across the board. He explained, with live drawings, how people working on this goal establish rules for each of the elements in graphical elements, such as app windows, and how that has contributed to apps gracefully integrating with each other.

Méven Car then talked about all the progress made by the people working on [the Wayland goal](https://community.kde.org/Goals/Wayland), and Vlad Zahorodnii covered the next steps of what would soon be arriving.

All the goal Champions, Aleix, Meven and Niccolò, got together with Adam Szopa and Lydia Pintscher and talked about their projects and how they help focus the efforts of the KDE community. Although, as Lydia pointed out, it is not the only way to get things done in KDE, it is a good way to start and understand what KDE's main concerns and aims are.

The morning ended with four lightning talks, in which Anupam Basak talked about how to use Qt with Python; Carl Schwan analyzed the health of the KDE Community with some very illuminating (and colorful!) graphics and charts; Andreas Cord-Landwehr talked about SPDX License Markers in KDE, that is markers placed inside code that identifies the license the code is distributed under and that can be read by a machine (saving a lot of money in legal fees!). Finally, David Redondo talked about Power Profiles in Plasma, that is, how Plasma manages the power it needs, the state of the battery and so on.

The evening talks started at 17:00 UTC with the Sponsors' Talks, where the companies and organizations that helped finance Akademy explained their products and services, and the role Open Source software plays in their businesses. Mbition, KDAB, Qt Company, GitLab, Fedora all delivered talks.

Later, in Room 1, Nicolas Fella used his talk "What's cooking for KDE Frameworks 6" to explain the work being carried out to build KF6. KF6 is the version of KDE Frameworks that will use, and be compatible with Qt6. In the talk, developers learned about upcoming changes and how they could contribute to the effort.

Meanwhile, in Room 2, in his talk "Staying Indoors - OSM indoor maps for KDE Itinerary", Volker Krause observed that providing information about the insides of buildings, such as big stations and airports, can be vital for travelers. Unfortunately, often the data you get from online services is confusing and overwhelming. Volker explained how KDE's Itinerary app splits up the information, making large buildings easier to navigate.

At 20 past six, Dan Leinir Turthra Jensen took over in Room 1 and told us their History of Software Distribution, and explored the many ways they have tried to get the software to users.

At the same time, in room 2, Daniel Vrátil, introduced us to C++ Coroutines, a new feature in C++ 20. He explained how coroutines can be used with certain Qt operations, such as DBus calls, network replies, and so on.

Then Massimo Stella presented Kdenlive, KDE's advanced video-editing software. In his talk, he covered Kdenlive's new features, such as the "smart" subtitler, the zoom bars in the timeline, and same-track transitions.

Meanwhile, in room 2, Kevin Ottens, in his presentation "KF6 the Architecture Overview - Time to Slice Things Up Yet Again", talked about how the KF5 offer was originally structured and how it has led to some issues over time. He then explained an idea that could help improve things while moving to KF6.

Back in room 1, Paul Brown and Aniqa Khokhar explained to attendees how they can take advantage of the Promo team in "Promo as a Service", and how KDE members could improve the visibility of their projects.

Meanwhile, in room 2, David Edmundson was delivering "Wayland for Qt application developers". In his talk, he explored what Wayland means for Qt application developers, some common traps and pitfalls and how to avoid them.

During the last session in room 1, Cornelius Schumacher explained in "Towards sustainable computing" what the impact computing had on our environment and talked about strategies KDE could adopt to become more efficient, and how developers could create apps that used less energy.

Finally, in room 2, Alexander Saoutkin used his talk "ARM Memory Tagging Extension - Fighting Memory Unsafety with Hardware" to explore the threats posed by memory unsafety in C++ programs and some new tools that help minimize them.

### Day 3

#### Working Groups, Plasma, Networks and the Life Story of a Born Hacker

The day started at 8:00 UTC sharp with four very interesting lightning talks. Kai Uwe Broulik talked about how to become productive using Plasma at home. He pointed out tips and tricks that allow you to avoid interruptions and improve your time working from your couch.

Niccolò Venerandi explained from experience how to grow a KDE Video Community. Niccolò runs a fledgling and upcoming YouTube channel and is advised on how to manage your content and even how to successfully monetize it.

Alexander Saoutkin talked about KIO FUSE and how it brings a slew of useful features that help integrate remote file systems into the local one.

Rohan Asokan talked about Kalk, his first OSC project and his first experience developing for KDE. His story was an uplifting look into how you can get started in Free Software development.

At 9:00 UTC, Aleix Pol, Adriaan de Groot, Eike Hein, Lydia Pintscher and Neofytos Kolokotronis delivered their traditional yearly live KDE e.V. Board report. The Board members gave an overview of the activities carried out over the last year and provided an outlook for the next. Highlights included employing more contractors to carry out vital KDE work, plans for upcoming events and sprints (in person at last!), and the plans for KDE's 25th Anniversary happening later this year.

This was followed by KDE's working groups' reports. Tomaz Canabrava kicked off things by telling us about the Community Working Group, the group of valiant volunteers who tackle conflict resolution within the Community. Although 2020 was rough for many reasons, the CWG managed to solve disputes and added a new member to their ranks.

Frederik Gladhorn and Olaf Schmidt-Wischhöfer told us about the KDE Free Qt Working Group. The KDE Free Qt Foundation is in charge of maintaining and managing the agreement between KDE and the Qt Company regarding the licensing as free software of the Qt framework. The working group is supporting the members of the foundation and helped set up [KDE's Qt 5 Patch Collection](https://dot.kde.org/2021/04/06/announcing-kdes-qt-5-patch-collection).

Bhushan Shah then introduced the work being carried out by the Sysadmin Working Group. The list of tasks and achievements completed by KDE's sysadmins over 2020 is too long and impressive to list here, but, the highlights include setting up the Big Blue Button infrastructure for KDE events and sprints, upgrading and modernizing KDE's server infrastructures so it can handle the increase in demands from the Community (it served 40 Terabytes only in April!), the implementation of MyKDE, which will soon substitute KDE Identity, and improved specialized services, like maps.kde.org, that serves maps and plans to KDE's mapping applications such as Marble and Itinerary.

Neofytos introduced the Advisory Board Working Group and explained its role as a point of contact for sponsors and members of the Advisory Board. Neofytos also introduced KDE's two new patrons: Pine64 and Slimbook.

The Financial Working Group reported that one time donations increased by 40% in 2020. These are donations made by private citizens. The income from student mentorships (GSoC, Code-In) decreased as the interest seems to be trending down for these activities. Meanwhile, major income sources remained stable, although financial assets grew in 2020. Hence, as a non-profit, KDE needs to spend more money and is currently doing so by starting to employ more contractors.

Carl Schwan introduced the Fundraising Working Group, the group that identifies fundraising opportunities. Carl introduced the new member of the group, Niccolò Venerandi, and told us about the updated relate.kde.org and donations pages.

Regular talks started again at 10:20 UTC, and Raghavendra Kamath told us how they built a new way for artists, users and developers to communicate in the Krita community using Discourse. Apart from the technical details of the implementation, Raghavendra explained different features and how they benefited the users.

Meanwhile, in room 2, Kai Köhne, from the Qt Company, talked about "Porting user applications to Qt" and how the release of Qt 6 is going. Kai explained that, although 6.0 was released in December, not all of Qt 5 was ported at that time. Gradually, over 2021, more and more Qt libraries and frameworks will be implemented into Qt6, reaching completion in early 2022.

Later, in room 1, Neofytos Kolokotronis delivered his talk "Developing products that break out of our bubble(s)" in which he presented the various levels of bubbles that KDE's products need to break out from in order to grow their userbase. He used examples from applications that are already doing well, and proposed candidates ready to travel outside KDE's orbit.

In room 2, Timothée Ravier introduced us to "Kinoite", a new immutable Fedora variant with the KDE Plasma desktop based on rpm-ostree, Flatpak and podman. Timothée explained how "Kinoite" improves the user experience with atomic and safe updates for the system (rpm-ostree), the applications (Flatpak) and development tools or containers (podman).

The morning sessions finished with Lydia Pintscher and Neofytos again in room 1 talking about "Making a living in KDE", and Arjen Hiemstra in room 2 with his presentation "Closing the distance between CPU and GPU with Signed Distance Fields".

At last year's Akademy, the KDE e. V. board announced that they were putting together an initiative to help people make a living with KDE products. In today's presentation, Lydia explained what they wanted to do in that regard and the plans for the future.

Meanwhile, in room 2, Arjen explained how traditional 2D rendering methods using exclusively CPU were missing out from using increasingly powerful GPUs. In his talk, Arjen described a technique called Signed Distance Fields, already in use in the KQuickCharts framework and ShadowedRectangle in Kirigami, that offers a very powerful tool for advanced 2D rendering.

After a break, the conference started up at 17:00 UTC again with sponsor talks. openSUSE, Canonical, reMarkable, Pine64, Shells and Collabora all sent video messages explaining what they do and wishing the Community a happy and productive Akademy.

In the first talk of the evening in room 1, Andreas Cord-Landwehr introduced us to "The Art of Logging", and showed us how the Qt logging framework worked and how one can access and analyze logs via remote access even using an embedded device, like a Plasma Mobile smartphone.

In room 2, Marco Martin talked about "Plasma internals: the next few years", where he talked about the move from Qt5 to Qt6. He explained that, while the port to Qt6 is not posing a technology challenge as big as when KDE migrated from Qt4 to Qt5, it does open the door to learning from the lessons of the Plasma 5 lifetime, and the possibility to refactor and simplify things in order to offer a leaner and more robust experience for users and developers alike.

At 20:20 UTC, David Edmundson presented "Addressing Wayland Robustness" in room 1, in which he talked about the current inherent instability of Wayland and how Plasma, as a late-adopter of the X Windows successor, could possibly avoid some of the issues that troubled other environments.

In room 2, Christian Strømme took us to another dimension with his talk "Qt Quick 3D in Qt 6.2". In his talk, Christian introduced us to Qt Quick 3D and its features and showed us how it could be used to create spectacular 3D renders and effects.

Later, Björn Balazs took to room 1 and told us "How we can solve the personal data problem". Given the issues derived from collecting and processing personal data, Björn provided in his talk a vision for the KDE Community of a system, that would be trustworthy, democratic, transparent and that guarantees digital privacy for each and every one. At the same time, it would provide fair access to personal data for those interested.

Meanwhile, in room 2 Igor Ljubuncic, aka "Dedoimedo", took us to Snaps, the final frontier with "Dev Trek - The Next Generation". In Igor's talk, he told us about the advantages of Snaps, self-contained applications, that boast reliable updates and a coherent behavior across a wide range of distributions.

Towards the end of the evening, Bhavisha Dhruve, Aniqa Khokhar, Aiswarya Kaitheri Kandoth and Tomaz Canabrava told us about KDE Network, the project that builds communities in places where Free Software adoption would otherwise be scattered at best. In the talk, the panel explained the achievements they have reached since the program began, and the upcoming projects they are currently working on.

The last session was the keynote by Jeri Ellsworth, "Journey from Farm Girl to Holograms". Jeri is a maker extraordinaire, ex-Valve hacker, self-taught chip designer and inventor of a system for playing 3D tabletop games using Augmented Reality glasses. She told us how her mentors helped her become the successful techie she is today in a roller coaster of a story, with so many twists and turns, that this brief description cannot do it justice.

From Monday to Thursday, Akademy attendees participated in Bird of a Feather (BoF) meetings, private reunions, and hackathons.

### Day 8

#### BoFs, Talks and Awards

After 4 days + 1 morning of BoFs, hacking sessions and meetings, talks at Akademy resumed in room 1 on Friday at 17:00 UTC. Kevin Ottens and Christelle Zouein from enioka Haute Couture kicked things off with the talk *Community's Adventures in Analyticsland - Or the State of the Community Through New Analytics*. Christelle and Kevin showed us the data analysis tools they have been working on to study information collected from KDE's development repositories and the surprising facts and trends they discovered.

At the same time, in room 2, Volker Krause talked about *Releasing Android Apps - Building, optimizing and deploying release APKs*. In this talk, Volker explained that, because KDE developers are producing more and more mobile-friendly applications, there was a need to better understand how to release mobile platforms.

Volker covered the efforts to expand KDE's tools for building packages (that already help produce packages for Windows, macOS and AppImage) so developers can also use them to create Android packages.

At 19:40, Manav Sethi and Paul Brown came on in room 1 to talk about a new KDE project: *Kockatoo* a tool to simplify the management of social media posting. After Paul explained the issues derived from managing multiple accounts on a wide variety of platforms, Manav demonstrated how Kockatoo can help. The speakers then explained what was missing from the project and how they thought Kockatoo could help the different KDE projects be more efficient on social media.

In room 2, Lars Knoll from the Qt Company, talked about Qt 6, its new features and the current roadmap leading toward its completion. In the talk, Lars gave an overview of the largest changes that are included in Qt 6, where the development stands right now with Qt 6.1 and where it is headed.

At 20:20, Albert Astals Cid talked about the KDE Qt 5.15 patch collection, the branch of Qt5 maintained by the KDE community after the Qt Company halted updates to concentrate on Qt6. Albert explained in his talk why this patch collection was created, what it is and how it is maintained.

Meanwhile, in room 2, Shawn Rutledge talked about *Interactive UIs in Qt Quick 3D*. In his talk, Shawn explored the possibilities of enabling controls and interactive elements in 3D virtual reality-like environments and showed us some seriously cool examples of how technology works.

At 21:00 UTC, Nuno Pinheiro took over in room 1 and talked of his work developing O² ("Oxygen squared"), a new icon set based upon KDE's iconic Oxygen designs of yore.

In room 2, Thomas Hartmann introduced us to *Qt Design Studio*, a new tool that intends to break the cycle of painstaking feedback loops between designers and developers.

After this talk in room 2, all the attendees convened in room 1 to hear Aleix Pol, president of KDE, talk about working professionally with KDE. In his presentation, Aleix shared his own experiences of working with the KDE Community and reflected on the experience of having hired several contractors to work within KDE.

The last act of Akademy 2021 after the BoF wrap up of the day, was the traditional ceremony of the Akademy Awards. This year they went to Alexander Semke of LabPlot for Best Application, Paul Brown from the Promo team for Best Non-Application Contribution, and Adriaan de Groot, who received the Jury Award for his selfless dedication and work within the KDE Community.

And that was it! Another fun-filled and fruitful Akademy was over, and now we all look forward to meeting next year again, hopefully in person this time.

All the talk recordings are now available on [PeerTube](https://conf.tube/videos/watch/playlist/38bfc51e-253d-4976-ba1c-1a16bcc6d9b0) or if you prefer you can also just grab the [files](https://files.kde.org/akademy/2021/)
