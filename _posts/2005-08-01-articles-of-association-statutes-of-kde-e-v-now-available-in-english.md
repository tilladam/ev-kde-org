---
title: 'Articles of Association (Statutes) of KDE e.V. now available in English'
date: 2005-08-01 00:00:00 
layout: post
---

An English translation of KDE e.V.'s Articles of Association is now <a href="http://ev.kde.org/corporate/statutes-en.php">available</a>.
