---
title: 'KDE e.V. Quarterly Report 2012 Q3'
date: 2012-11-28 00:00:00 
layout: post
---

KDE e.V., the non-profit organisation supporting the KDE community, is happy to present the <a href="http://ev.kde.org/reports/ev-quarterly-2012_Q3.pdf">third quarterly report of 2012</a>.

This report covers July through September 2012, including statements from the board, reports from sprints, financial information and a report from this year's Akademy, our annual conference. This report also marks the 15th birthday of KDE e.V.
      
