---
title: 'KDE e.V. is looking for documentation support'
date: 2018-08-30 00:00:00 
layout: post
noquote: true
---

> Applications for this position **are closed**.

KDE e.V., the non-profit organisation supporting the KDE community, is looking for a technical writer to help KDE improve its documentation. Please see the <a href="https://ev.kde.org/resources/documentationsupport-callforproposals.pdf">call for proposals</a> for more details about this contract opportunity. We are looking forward to your application.
      
