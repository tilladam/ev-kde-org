---
title: 'KDE e.V. looking for an event manager'
date: 2014-02-28 00:00:00 
layout: post
---

> Applications for this position **are closed**.

KDE e.V., the non&#173;profit organisation supporting the KDE community, is looking for an event manager for Akademy 2014 at the earliest possible date.

If you are looking for the opportunity to drive the organisation of Akademy and bring together one of the greatest Free Software communities, please send us your application.

More details about the job and how to apply can be found <a href="http://ev.kde.org/resources/eventmanager-advert.pdf">here</a>.
      
