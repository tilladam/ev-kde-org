---
title: 'KDE e.V. Report for 2nd Half of 2015'
date: 2016-12-10 00:00:00 
layout: post
---

KDE e.V., the non-profit organisation supporting the KDE community, is happy to present the <a href="https://ev.kde.org/reports/ev-2015H2/" target="_blank">report for the second half of 2015</a>.

This report covers July through December 2015, including statements from the board, reports from sprints and a feature article about The KDE Incubator.
      
