---
title: 'KDE e.V. General Assembly 2009'
date: 2009-07-08 00:00:00 
layout: post
---

On July 7th the general assembly of KDE e.V. was held. A new
board was elected. Frank Karlitschek and Celeste Lyn Paul joined after
Aaron Seigo and Klaas Freitag leaving the board.
<a href="http://dot.kde.org/2009/07/07/kde-ev-elects-new-board-directors">Read more...</a>
