---
title: 'Changes in Qt offering and the KDE Free Qt Foundation'
date: 2020-04-06 04:04:04 
layout: post
---

The Qt Company has recently adopted [new policies](https://www.qt.io/blog/qt-offering-changes-2020) regarding binaries and Long Term Support of their Qt framework product for Q1 of 2020. The installation of Qt binaries will require a Qt Account and the long-term-supported (LTS) releases and the offline installer will become available to commercial licensees only. It’s worth noting that The Qt Company has publicly stated it will support Qt 5.12 under a free license until December 2021. 

Even though the new policies might be legitimate for a for-profit enterprise, they have the unfortunate secondary effect of putting at risk benefits historically enjoyed by users of the non-commercial branch of Qt, in addition to the privacy and security concerns that are raised. This is a challenge to FOSS developers, but KDE remains strong, as our community are assessing the situation and plan for the best path forward through the [KDE Free Qt Foundation](https://kde.org/community/whatiskde/kdefreeqtfoundation.php) (KFQF).

The KDE Free Qt Foundation is an organization with the purpose of securing the availability of the Qt toolkit for the development of Free Software. Over the years the Foundation has worked on laying down the ground rules to ensure Qt remains free and compatible with the values of FOSS. The successive versions of the agreement drawn between the owners of the commercial version of Qt and the Foundation have ensured that Qt remained free for all developers and added a provision that, if triggered, would entitle KDE to release Qt under a BSD license.
	
Talks between the KDE and The Qt Company members within the KDE Free Qt Foundation are ongoing to ensure that the agreement is respected. The KDE e.V. and the larger community of KDE contributors support and trust the KDE Free Qt Foundation to always seek the most beneficial outcome for all Free Software developers and advocates.

Regardless of the future developments in this area, KDE continues to move forward with the guarantee that the KDE Free Qt Foundation will defend the rights of Free Software developers and Open Source software with regards to the Qt tool set.
